@Metrica.module "CanvasApp.Draw", (Draw, App, Backbone, Marionette, $, _, CanvasApp) ->

	Draw.Controller = 
		canvas: null

		drawLine: (points, normalized)->
			canvas = @canvas[0]
			ctx = canvas.getContext "2d"
			ctx.beginPath()
			
			xFactor = if normalized then canvas.width  else 1
			yFactor = if normalized then canvas.height else 1

			ctx.moveTo points[0].x * xFactor, points[0].y * yFactor
			ctx.lineTo points[1].x * xFactor, points[1].y * yFactor, 10
			
			ctx.strokeStyle = '#E01A00'
			ctx.lineWidth = 2
			ctx.stroke()

		drawLineWhileDrawing: (point, mousePosition)->
			x = point.x * @canvas[0].width
			y = point.y * @canvas[0].height
			
			@clearCanvas()
			@drawPoint({x:x,y:y})
			@drawLine([mousePosition, {x:x,y:y}], false)
			

		clearCanvas: ->
			ctx = @canvas[0].getContext "2d"
			ctx.clearRect 0, 0, @canvas[0].width, @canvas[0].height

		drawPoint: (point)->
			ctx = @canvas[0].getContext "2d"
			ctx.beginPath()
			ctx.fillStyle = '#E01A00'
			ctx.arc point.x, point.y, 5, 0, 2 * Math.PI, false
			ctx.fill()
			
		getPoint: (e) ->
			x = e.offsetX - @canvas[0].offsetLeft + 20
			y = e.offsetY - @canvas[0].offsetTop  + 20
			return {x: x, y: y}			 	 
		

 
				