@Metrica.module "CanvasApp.Show", (Show, App, Backbone, Marionette, $, _) ->

	Show.Controller = 
		
		showCanvas: ->
			canvasView = @getCanvasView()
			App.canvasRegion.show canvasView
			return canvasView.ui.$canvas
		
		getCanvasView: ->
			new Show.Canvas


 
				