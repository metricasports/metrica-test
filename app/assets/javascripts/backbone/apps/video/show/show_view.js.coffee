@Metrica.module "VideoApp.Show", (Show, App, Backbone, Marionette, $, _) ->
	class Show.Video extends Marionette.ItemView
		template: "video/show/templates/show_video"
		ui:
			'$videoPlayer' : '#video-player'
		
		initialize: ->
			lastSecondCaptured: 0
			currentSecondCaptured: 0

		onRender: ->
			videojs(@ui.$videoPlayer[0], {})
			App.vent.trigger "setApi:video", @$('#video-player_html5_api')
			@addVideoTimeUpdateListener()
			
		addVideoTimeUpdateListener: ->
  			@$('#video-player_html5_api').on 'timeupdate', (t) =>
  				@currentSecondCaptured = parseInt(t.target.currentTime)
  				@sendSyncEventEverySecond()	
  			
  			@$('#video-player_html5_api').on 'playing', (e) ->
  				App.execute "playing:video"	
		
		sendSyncEventEverySecond: ->
			if @lastSecondCaptured != @currentSecondCaptured 
				@lastSecondCaptured = @currentSecondCaptured
				App.vent.trigger("syncPulse:video", @currentSecondCaptured)




		