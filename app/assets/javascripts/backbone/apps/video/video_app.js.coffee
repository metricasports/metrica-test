@Metrica.module "VideoApp", (VideoApp, App, Backbone, Marionette, $, _) ->
	
	API =
		showVideo: ->
			VideoApp.Show.Controller.showVideo()

	VideoApp.on 'start', ->
		@startWithParent = false
		@videoApi = null
	
		API.showVideo()

	App.vent.on 'setApi:video', (v) ->
		VideoApp.videoApi = v

	App.commands.setHandler 'drawing:line', ->
		VideoApp.videoApi[0].pause()
	
	App.vent.on 'clicked:addLine', ->
		VideoApp.videoApi[0].pause()

	App.reqres.setHandler 'currentTime:video', ->
		parseInt(VideoApp.videoApi[0].currentTime)