@Metrica.module "InputApp.List", (List, App, Backbone, Marionette, $, _) ->

	List.Controller = 
		
		lines: null
		currentLine: null

		listInput: ->
			@lines = App.request "line:entities"
			inputView = @getInputView @lines
			App.inputRegion.show inputView

		getInputView: (lines)->
			new List.Inputs
				collection: lines

		_addLine: ->
			time = App.request "currentTime:video"
			model = @lines.find (m)->
				m.get("time") == time
			if model
				@currentLine = model
			else
				@currentLine = @lines.add {name: "Line" + (@lines.length + 1), time: time}
			
	App.vent.on 'clicked:addLine', =>
		@Controller._addLine()
		App.vent.trigger "getReadyForDrawing:canvas" 
	
	App.reqres.setHandler 'currentLine:lines', ->
		return List.Controller.currentLine	
